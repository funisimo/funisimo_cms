$(document).ready(function () {
    $(window).load(function () {
        function printElementAndIndex( elem, index ) {
             console.log(index + ": " + elem);
        }
        function addActionEvent(Img1,Img2,ratio){
            r = ratio.split(':');
            rat=r[0]/r[1];
            SetDefault(Img1,Img2,rat,ratio);
            SetDefault(Img2,Img1,rat,ratio);
        }
        function SetDefault(img,img2,rat,ratio){
            img_h = $(img).height();
            img_w = $(img).width();
            img2_h = $(img2).height();
            img2_w = $(img2).width();
            ratio_h = img2_h / img_h;
            ratio_w = img2_w / img_w;
            ratio_h2 = img_h / img2_h;
            ratio_w2 = img_w / img2_w;
            w = img_w;
            h = w / rat;
            if (h > img_h) {
                h = img_h;
                w = h * rat;
            }
            x1 = (img_w - w) / 2;
            y1 = (img_h - h) / 2;
            x2 = w + x1;
            y2 = h + y1;
            $(img).imgAreaSelect({
                x1: x1, y1: y1, x2: x2, y2: y2,
                minWidth: 20, minHeight: 20,aspectRatio: ratio, handles: true,
            })
            writeOutput(img,x1,x2,y1,y2);
            addEvent(img,img2,x1,x2,y1,y2,rat,ratio,ratio_w2,ratio_h2);
        }
        function addEvent(image1,image2,x1,x2,y1,y2,rat,ratio,dif_w,dif_h){
             
            $(image1).imgAreaSelect({
                x1: x1, y1: y1, x2: x2, y2: y2,
                minWidth: 20, minHeight: 20,aspectRatio: ratio, handles: true,
                onSelectChange: function (img, selection) {
                    v1 = selection.x1/dif_w;
                    v2 = selection.x2/dif_w;
                    v3 = selection.y1/dif_h;
                    v4 = selection.y2/dif_h;
                    writeOutput(img,selection.x1,selection.x2,selection.y1,selection.y2);
                    SetValues(image2, v1, v2, v3, v4,ratio);
                },
                onSelectEnd: function (img, selection) {
                    writeOutput(selection.x1,selection.x2,selection.y1,selection.y2);
                    if(selection.x2 - selection.x1 == 0){
                        addActionEvent(image1,image2,ratio);
                    }
                }
            });
        }
        function writeOutput(img,x1,x2,y1,y2){
            $(img).siblings(".output").html(x1+" "+x2+" "+y1+" "+y2);
        }
        function SetValues(img,x1,x2,y1,y2){
            writeOutput(img,x1,x2,y1,y2);
            $(img).imgAreaSelect({
                x1: x1, y1: y1, x2: x2, y2: y2,
            })
        }
        // Img1(id),Img2(id),ratio (String) - example '4:3'
        addActionEvent('#img3','#img4','8:3');
        addActionEvent('#img','#img2','4:3');
    });
});