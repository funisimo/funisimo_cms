<?php

class PageC extends BaseController {

	public function getIndex()
	{
		return View::make('start');
	}
	public function getHome(){
		return View::make('page.home');
	}
	public function getSearch(){
		
		return View::make('page.tw_search');
	
	}
	public function getMentions(){
		
		return View::make('page.tw_mentions');
	
	}
	public function getTw(){
		
		return View::make('page.tw_users');
	
	}
	public function postTw()
	{
		return View::make('page.tw_code');
	}
	public function getFake()
	{
		App::bind('foo', function($app)
		{
		    return new Home;
		});
	}
}