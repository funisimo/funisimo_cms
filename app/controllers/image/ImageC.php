<?php

class PageC extends BaseController {

	function __construct() {
		private constant('folder','img');
	}

	public function getIndex()
	{
		return 'image controller';
	}
	public function postSave(){
		$fileProperties = Input::all();
		if($_FILE){
			if($fileProperties['x1'] && $fileProperties['y1'] && $fileProperties['x2'] 
			   && $fileProperties['y2'] && $fileProperties['width'] && $fileProperties['height']){	
				$dst_x = 0;
				$dst_y = 0;
				$src_x = $fileProperties['x1']; // Crop Start X
				$src_y = $fileProperties['y1']; // Crop Srart Y
				$dst_w = $fileProperties['x2']; // Thumb width
				$dst_h = $fileProperties['y2']; // Thumb height
				$src_w = $fileProperties['width']; // $src_x + $dst_w
				$src_h = $fileProperties['height']; // $src_y + $dst_h

				$dst_image = imagecreatetruecolor($dst_w,$dst_h);
				$src_image = imagecreatefromjpeg("images/source.jpg");
				imagecopyresampled($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
				if(imagejpeg($dst_image, constant('folder')."/crop.jpg")) $cond = 'success - Img stored in: '.constant('folder')."/crop.jpg";
			}else $cond = 'Some variables not specified!';
		}else $cond = 'no file';
		return $cond;
	}
	public function getImageUrl()
	{
		$img['name'] = Input::get();
		if($img['name']){
			$img['url'] = constant('folder').'/'.$img['name'].'.jpg';
		}
		return $img;
	}
}

