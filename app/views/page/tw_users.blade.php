@extends('start')

@section('content')
<script type="text/javascript" src="js/2-jquery.js"></script>

<h1>twitter page</h1>

    <?php

//function takes the time from the tweet and computes a "time ago"
function time_elapsed_string($ptime) {

    $etime = time() - $ptime;

    if ($etime < 1) {
        return '0 seconds';
    }

    $a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
                );

    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
        }
    }
}

//makes urls out of links, @usernames, and hash tags.
function makeURLs($text) {
        // Match URLs
        $text = preg_replace('`\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))`', '<a href="$0">$0</a>', $text);

        // Match @name
        $text = preg_replace('/(@)([a-zA-Z0-9\_]+)/', '@<a href="https://twitter.com/$2">$2</a>', $text);
                                                        
        // Match #hashtag
        $text = preg_replace('/(#)([a-zA-Z0-9\_]+)/', '<a href="https://twitter.com/search/?q=%23$2">#$2</a>', $text);
        
        return $text;
        
}

//the magic. Returns the feed in a unorder listed.
//to use - echo getTwitterFeed();
function getTwitterFeed () {
        //need to create an app in twitter dev and get OAuth codes. Put them here
        $token = '1529970560-5s7M7VefaAv06gHmXCsC7I1JQyInF9Ep4Mf5eM9';
        $token_secret = 'KRnxK8g7p5WN9UA8oqoHft1xXFZ12VOL5BVpRgHAQ';
        $consumer_key = 'Z6HIziHA3jEZ9cwGOs2zMQ';
        $consumer_secret = '0Wfj7NO8N6nIb6iQk67OUiPhsbFSHD9D6BWpBGaSQo';
        

        $method = 'GET';
        $host = 'https://api.twitter.com/1.1/users/search.json'; // api call path
        
        //edit these too.
        $query = array( // query parameters
            'q' => 'latvia',
            'text' => 'tlc'
            //'text' => 'lol',
            //'count' => '5' //0 returns all (limit is 200 i think),
            //'q' => 'noradio',
             //          'page' => '3'
        );
        
        $oauth = array(
            'oauth_consumer_key' => $consumer_key,
            'oauth_token' => $token,
            'oauth_nonce' => (string)mt_rand(), // a stronger nonce is recommended
            'oauth_timestamp' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_version' => '1.0'
        );
        
        $oauth = array_map("rawurlencode", $oauth); // must be encoded before sorting
        $query = array_map("rawurlencode", $query);
        
        $arr = array_merge($oauth, $query); // combine the values THEN sort
        
        asort($arr); // secondary sort (value)
        ksort($arr); // primary sort (key)
        
        // http_build_query automatically encodes, but our parameters
        // are already encoded, and must be by this point, so we undo
        // the encoding step
        $querystring = urldecode(http_build_query($arr, '', '&'));
        
        $url = "$host";
        
        // mash everything together for the text to hash
        $base_string = $method."&".rawurlencode($url)."&".rawurlencode($querystring);
        
        // same with the key
        $key = rawurlencode($consumer_secret)."&".rawurlencode($token_secret);
        
        // generate the hash
        $signature = rawurlencode(base64_encode(hash_hmac('sha1', $base_string, $key, true)));
        
        // this time we're using a normal GET query, and we're only encoding the query params
        // (without the oauth params)
        $url .= "?".http_build_query($query);
        $url=str_replace("&amp;","&",$url); //Patch by @Frewuill
        echo $url.'<br>';

        $oauth['oauth_signature'] = $signature; // don't want to abandon all that work!
        ksort($oauth); // probably not necessary, but twitter's demo does it
        
        // also not necessary, but twitter's demo does this too
        function add_quotes($str) { return '"'.$str.'"'; }
        $oauth = array_map("add_quotes", $oauth);
        
        // this is the full value of the Authorization line
        $auth = "OAuth " . urldecode(http_build_query($oauth, '', ', '));
        
        // if you're doing post, you need to skip the GET building above
        // and instead supply query parameters to CURLOPT_POSTFIELDS
        $options = array( CURLOPT_HTTPHEADER => array("Authorization: $auth"),
                          //CURLOPT_POSTFIELDS => $postfields,
                          CURLOPT_HEADER => false,
                          CURLOPT_URL => $url,
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_SSL_VERIFYPEER => false);
        
        // do our business
        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);
        curl_close($feed);
        
        $twitter_data = json_decode($json,true);
        
                //print_r($twitter_data);
        
        if (empty($twitter_data)) {
                $code = 'There was an error';
        } else {
                $code = '<ul id="twitterFeed">';
                
                d($twitter_data);
                // $Obj =  (object) $twitter_data;
                // d($Obj);
                // foreach ($Obj as $key => $value) {
                //     d($value->status());
                // }
                // if(isset($twitter_data['statuses'])){
                //    foreach ($twitter_data['statuses'] as $keys ) {
                //     foreach ($keys as $key => $value) {
                //         if($key == 'profile_background_image_url'){
                //             echo $value.'<br>';
                //         }
                //     }
                //    }
                // }
                
                   
                    // if(isset($tweets['metadata']['id'])){
                    //     echo $tweets['metadata']['id'];
                    // }
                    
                        // $text = makeURLs($tweets['text']);
                        // $time = strtotime($tweets['created_at']);
                        // $url = 'http://twitter.com/'.$tweets['user']['screen_name'].'/status/'.$tweets['id'];
                        // $agoTime = time_elapsed_string($time);
                        // $code .= '<li>';
                        // $code .= '<span class="twitter-date"><a href="'.$url.'">'.$agoTime .'</a></span>';
                        // $code .= '<br />';
                        // $code .= '<span class="twitter-text">'. $text . '</span>';
                        // $code .= '</li>';
                    
                
        
                $code .= '</ul>';
        }
        
        return $code;

}

//for testing
echo getTwitterFeed();
?>



{{HTML::script('js/tw.js')}}

@stop