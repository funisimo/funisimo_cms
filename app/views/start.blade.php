
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Laravel PHP Framework</title>
  
    
</head>
<body>
    <div class="welcome">
    	@section('links')
    	{{HTML::link('/','start')}}
    	<br>
        {{HTML::link('home','Home')}}
        <br>
        {{HTML::link('search','Twitter Search')}}
        <br>
        {{HTML::link('mentions','Twitter Mentions')}}
        <br>
        {{HTML::link('tw','Twitter Users')}}
        <br>
        <h1>Start page.</h1>

        @show
        <div class="content">
        @section('content')       
        @show
        </div>
    </div>
</body>
</html>
