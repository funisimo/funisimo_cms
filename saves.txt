class { 'vhost':
    name => 'platform',
    wordpress => 'latest',
  }

  	$git_clean    = true
	$git_pull     = true

   vhost::get {
    # Platform Themes
    '/wp-content/themes/dni_wp_theme_platform_base': git_github => 'DNI-New-Platform/dni_wp_theme_platform_base.git', version => 'development';
    '/wp-content/themes/dni_wp_theme_animal_planet': git_github => 'DNI-New-Platform/dni_wp_theme_animal_planet.git', version => 'development';
    '/wp-content/themes/dni_wp_theme_tlc_uk': git_github => 'DNI-New-Platform/dni_wp_theme_tlc_uk.git', version => 'development';
    '/wp-content/themes/dni_wp_theme_discovery_marketing': git_github => 'DNI-New-Platform/dni_wp_theme_discovery_marketing.git', version => 'development';
    '/wp-content/themes/dni_wp_theme_parent': git_github => 'DNI-New-Platform/dni_wp_theme_parent.git', version => 'development';
    '/wp-content/themes/dni_wp_theme_tlc_ua': git_github => 'DNI-New-Platform/dni_wp_theme_tlc_ua.git', version => 'development';
    '/wp-content/themes/dni_wp_theme_tlc_kr': git_github => 'DNI-New-Platform/dni_wp_theme_tlc_kr.git', version => 'development';
    # Platform plugins
    '/wp-content/plugins/dni_wp_plugin_ad_management_tool': git_github => 'DNI-New-Platform/dni_wp_plugin_ad_management_tool.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_platform_extensions': git_github => 'DNI-New-Platform/dni_wp_plugin_platform_extensions.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_poll': git_github => 'DNI-New-Platform/dni_wp_plugin_poll.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_core': git_github => 'DNI-New-Platform/dni_wp_plugin_core.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_carrington_build_extensions': git_github => 'DNI-New-Platform/dni_wp_plugin_carrington_build_extensions.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_carrington_build_modules': git_github => 'DNI-New-Platform/dni_wp_plugin_carrington_build_modules.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_settings_manager': git_github => 'DNI-New-Platform/dni_wp_plugin_settings_manager.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_carrington_build': git_github => 'DNI-New-Platform/dni_wp_plugin_carrington_build.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_photogallery': git_github => 'DNI-New-Platform/dni_wp_plugin_photogallery.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_content_migration2': git_github => 'DNI-New-Platform/dni_wp_plugin_content_migration2.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_user_role_editor': git_github => 'DNI-New-Platform/dni_wp_plugin_user_role_editor.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_image_store': git_github => 'DNI-New-Platform/dni_wp_plugin_image_store.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_site_settings': git_github => 'DNI-New-Platform/dni_wp_plugin_site_settings.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_flexible_templates': git_github => 'DNI-New-Platform/dni_wp_plugin_flexible_templates.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_post_ratings': git_github => 'DNI-New-Platform/dni_wp_plugin_post_ratings.git', version => 'development';
    '/wp-content/plugins/dni_wp_plugin_multiple_post_thumbnails': git_github => 'DNI-New-Platform/dni_wp_plugin_multiple_post_thumbnails.git', version => 'development';
    # WP Plugins
    '/wp-content/plugins/user-switching': svn_wordpress => 'user-switching';
    '/wp-content/plugins/wordpress-importer': svn_wordpress => 'wordpress-importer';
    '/wp-content/plugins/debug-bar': svn_wordpress => 'debug-bar';
    '/wp-content/plugins/debug-bar-actions-and-filters-addon': svn_wordpress => 'debug-bar-actions-and-filters-addon';

  }

